# docker-php-dev

A plain cli php image with Xdebug and Composer installed.
When developing in PHPStorm I use this as an _Remote Interpreter_ to run Xdebug, Composer, PHPUnit, PHP CodeSniffer, PHPMD. Full applications would need a docker-compose setup with webserver and database, but for small libraries or cli tools this is just fine.
